import 'dart:convert';

class User {
  String _username;
  String _password;
  int _roleId;
  User(this._username, this._password, this._roleId);

  User.map(dynamic obj) {
    this._username = obj["username"];
    this._password = obj["password"];
    this._roleId = obj["RoleId"];
  }

  String get username => _username;
  String get password => _password;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = _username;
    map["password"] = _password;

    return map;
  }
}
class PostBaseData{
  String clientKey;
  PostBaseData(this.clientKey);
  PostBaseData.map(dynamic obj){
    this.clientKey = obj["AppKey"];
  }
  String get clientkey => clientKey;
  Map<String, dynamic> toMap(){
    var map = new Map<String, dynamic>();
    map["AppKey"] = clientkey;
    return map;
  }
}
//  class ConFigData{
//    final String clientKey = "app_key";
//    final String clientType = "Mobile-iOS";
//    final String isrequestFromKey = json.encode({bool:false});
//  }
class Post{
  final String clientKey;
  final String clientType;
  final String isrequestFromWeb = json.encode({false});
  Post({this.clientKey, this.clientType, isrequestFromWeb });
  factory Post.fromJson(Map<String, dynamic> json){
    return Post(
      clientKey: json['ClientKey'],
      clientType: json['ClientType'],
      isrequestFromWeb: json['isRequestFromWeb'],
      
    );
  }
  Map toMap(){
    var map = new Map<dynamic, dynamic>();
    map["ClientKey"] = clientKey;
    map["ClientType"] = clientType;
    map["isRequestFromWeb"] = isrequestFromWeb;
    return map;
  }
}