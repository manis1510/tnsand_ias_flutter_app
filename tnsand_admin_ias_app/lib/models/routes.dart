// import 'package:flutter/widgets.dart';
// import 'package:tnsand_admin_app/home/home_screen.dart';
// import 'package:tnsand_admin_app/main.dart';
// import 'package:tnsand_admin_app/login/login_screen.dart';

// final routes = {
// '/main': (BuildContext context) => new MyApp(),
// '/home': (BuildContext context) => new HomeScreen(),
// '/': (BuildContext context) => new MyApp(),
// };
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tnsand_admin_ias_app/main.dart';

const String homeRoute = '/';
const String feedRoute = '/feed';


class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeRoute:
        return MaterialPageRoute(builder: (_) => Home());
      case feedRoute:
        return MaterialPageRoute(builder: (_) => Feed());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}