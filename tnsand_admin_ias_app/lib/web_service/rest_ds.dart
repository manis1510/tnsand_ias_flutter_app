import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tnsand_admin_ias_app/models/user.dart';
import 'package:tnsand_admin_ias_app/web_service/network_util.dart';
class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = "https://tnsandtestapi.azurewebsites.net/api/app/verify";
  static final LOGIN_URL = BASE_URL + "/login.";
  static final Auth_KEY = "";

 Map<String, String> requestHeaders = {
       'Content-type': 'application/json',
     };
  Future<User> login(String username, String password) {
    return _netUtil.post(LOGIN_URL, body: {
      "token": Auth_KEY,
      "username": username,
      "password": password
    }).then((dynamic res) {
      print(res.toString());
      if(res["error"]) throw new Exception(res["error_msg"]);
      return new User.map(res["user"]);
    });
  }
  Future<PostBaseData> authen(String clientKey)
  {
    
     var body = {
       'Appkey': clientKey,
     };
     var headers =  {
       'Content-type': 'application/json'
     };

      return _netUtil.post(BASE_URL,headers:headers,body:json.encode(body)).then((dynamic res){
        print(res.toString());
        if(res["error"]) throw new Exception(res["error_msg"]);
        return new PostBaseData.map(res["true"]);
      });

  }
  
   Map getApiHeader() {
    Map<String, String> headerMap = Map();
    headerMap["Content-Type"] = "application/json";
    return headerMap;
  }
  Future<Post> createPost(String url, header, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return Post.fromJson(json.decode(response.body));
  });
}
}