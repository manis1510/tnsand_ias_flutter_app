import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tnsand_admin_ias_app/models/user.dart';
class NetworkUtil {
  
  static NetworkUtil _instance = new NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<dynamic> get(String url) {
    return http.get(url).then((http.Response response) {
   final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      return _decoder.convert(res);
    });
  }
  // Future<dynamic> get(String url, String s, {Map<dynamic, String> headers, encoding}) {

  //   return http
  //       .get(
  //     url,
  //     headers: headers,
  //   )
  //       .then((http.Response response) {
  //     String res = response.body;
  //     int statusCode = response.statusCode;
  //     print("API Response: " + res);
  //     if (statusCode < 200 || statusCode > 400 || json == null) {
  //       res = "{\"status\":"+
  //           statusCode.toString() +
  //           ",\"message\":\"error\",\"response\":" +
  //           res +
  //           "}";
  //       throw new Exception( statusCode);
  //     }
  //     return _decoder.convert(res);
  //   });
  // }
  Future<Post> createPost(String url, hea, {header, body, Map headers}) async{
   return http.post(url,headers: header, body: body).then((http.Response response) {
  final int statusCode = response.statusCode;
  if(statusCode < 200 || statusCode > 400 || json == null) {
    throw new Exception("Error while fetching data ");
  }
  if(statusCode==200){}
  return Post.fromJson(json.decode(response.body));
});
}
  Future<dynamic> post(String url, {headers, body, encoding}) {
    return http
        .post(url, body: body, headers: headers, encoding: encoding)
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      return _decoder.convert(res);
    });
  }
}